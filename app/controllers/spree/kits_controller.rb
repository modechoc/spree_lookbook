module Spree
  class KitsController < Spree::StoreController
    respond_to :js, :html
    before_action :find_taxon, :except => :show_single_kit

    def show_single_kit
      @kit = Spree::Kit.friendly.find(params[:slug])

      if @kit.lookbooks.any? and @kit.taxons.any?
        lookbook = @kit.lookbooks.first
        redirect_to show_kit_path(@kit.taxons.first, lookbook, @kit), :status => :see_other and return
      end
    end

    def show

      if params[:slug]
        @lookbook = Spree::Lookbook.friendly.find(params[:slug])
      else
        @lookbook = Spree::Lookbook.joins(:kits).order(created_at: :desc).first
      end
      if params[:kit]
        @kit = @lookbook.kits.friendly.find(params[:kit])
      else
        @kit = @lookbook.kits.first
      end

      if request.path != show_kit_path(@taxon, @lookbook, @kit)
        redirect_to show_kit_path(@taxon, @lookbook, @kit), :status => :moved_permanently and return
      end
    end

    private
    def find_taxon
      @taxon = Spree::Taxon.friendly.find(params[:taxon])
    end
  end
end