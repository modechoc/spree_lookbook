Spree::Core::Engine.add_routes do
  get '/:taxon/inspirez-vous/:slug', to: 'lookbooks#show', as: :show_lookbook
  # get '/:taxon/inspirez-vous/:slug/:kit', to: 'kits#show', as: :show_kit
  get '/kits/:slug', :to => 'kits#show_single_kit', :as => :show_single_kit
  # route globbing for pretty nested taxon and product paths
  get '/*taxon/inspirez-vous/:slug/:kit', :to => 'kits#show', :as => :show_kit


  namespace :admin do
    get '/kits/add_product/', to: 'kits#add_product'
    get '/lookbooks/kits/add_product/', to: 'lookbooks/kits#add_product'
    get '/lookbooks/add_kit/', to: 'lookbooks#add_kit'



    concern :positionable do
      collection do
        post :update_positions
      end
    end

    resources :kits    do
      collection do
        get :search
      end
    end

    resources :kits, concerns: :positionable, controller: 'kits' do
      #admin_kit_update_product_position_path
      post "update_product_position", to: "kits#update_product_positions", as: :update_kits_product_position
    end
    resources :lookbooks, concerns: :positionable, path: "LB" do
      resources :kits, concerns: :positionable, controller: "lookbooks/kits" do
        # admin_lookbook_kit_update_product_position_path
        post "update_product_position", to: "lookbooks/kits#update_product_positions", as: :update_product_position
      end
      post "edit", to: "lookbooks#edit order"
    end



  end
end
